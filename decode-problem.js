module.exports = problem => {
  const types = ["intermediates", "start", "end"];
  const holds = {};
  types.forEach(type => (holds[type] = []));

  if (problem instanceof Buffer) problem = problem.toString("hex");
  if (typeof problem === "number") problem = problem.toString(16);

  if (problem.length > 0 && problem.length % 4 !== 0) {
    console.error("Invalid problem", problem);
    return holds;
  }

  for (let i = 0; i < problem.length; i += 4) {
    const index = parseInt(problem.substr(i, 2), 16);
    const type = parseInt(problem.substr(i + 2, 2), 16);
    const typeName = types[type];

    const letValue = index % 11;
    const numValue = Math.floor(index / 11) + 1;

    holds[typeName].push(String.fromCharCode(letValue + 65) + numValue);
  }

  return holds;
};
