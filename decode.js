const fs = require('fs');
const Knex = require("knex");
const decodeProblem = require("./decode-problem");

const knex = Knex({
  client: "sqlite3",
  connection: {
    filename: "./data/moonboardDB.sqlite"
  }
});

(async () => {
  const problems = await knex.select().from("problems");

  problems.forEach(problem => {
    problem.holds = decodeProblem(problem.holds);
  });

  const file = './data/problems.json';
  fs.writeFileSync(file, JSON.stringify(problems, null, 2));
  console.log("Wrote", problems.length, "problems to", file);

  process.exit();
})();
